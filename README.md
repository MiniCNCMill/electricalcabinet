# Genutzte Software
- [QElectroTech](https://qelectrotech.org/) for Wirering-Diagram
- [draw.io](https://draw.io) for Block-Diagram


# Starten der Anlage
1. Hauptschalter einschalten
2. Ordnungsgemäse Sicherheit überprüfen
    1. Sind alle Kabelverbindungen unbeschädigt und eingesteckt?
        1. Motor Kabel
        2. Sensor Kabel
        3. Verbindung zum Steuerpult
        4. Verbindung zum Rechner
    2. Befindet sich keine Person im Wirkungsbereich der Maschine
    3. Sind alle Not-Aus Melderschalter in der ausgefahrenen Stellung?
    4. "Sicherheit bestätigen" Taste drücken.
    5. Erlischt die Meldeleuchte im Taster?
3. Steuerrechner Starten
    1. Powerbutton am PC betätigen
    2. [Warten bis das OS gestartet wurde]
    3. ?? Anmelden
    4. Mach3 Software starten
4. "Steuerung Einschalten"-Taster betätigen
5. "RESET" in Mach3 betätigen
6. Referenzfahrt durchführen

# Notaus zurücksetzen
1. Ordnungsgemäse Sicherheit überprüfen
    1. Sind alle Kabelverbindungen unbeschädigt und eingesteckt?
        1. Motor Kabel
        2. Sensor Kabel
        3. Verbindung zum Steuerpult
        4. Verbindung zum Rechner
    2. Befindet sich keine Person im Wirkungsbereich der Maschine
    3. Sind alle Not-Aus Melderschalter in der ausgefahrenen Stellung?
    4. "Sicherheit bestätigen" Taste drücken.
    5. Erlischt die Meldeleuchte im Taster?
2. "Steuerung Einschalten"-Taster betätigen
3. "RESET" in Mach3 betätigen
4. Referenzfahrt durchführen

# Anlage ausschalten
1. "Steuerung Ausschalten"-Taster betätigen
2. Rechner ausschalten
    1. Mach3 Software beenden
    2. Rechner herrunterfahren
    3. [Warten bis der Rechner selbstständig ausgeschaltet ist]
3. Hauptschalter ausschalten

# Sicherheiten
## Spindel
### Signalfunktion
Die Spindel hat nur eine Spannungsversorgung.
### Sicherheitsaufbau
Die Spannungsversorgung wird 2-fach getrennt.
1. Hauptschütz der Anlage der Abgesichert ist durch eine Selbsthaltung mit integriertem Notausrelay
2. Steuerschütz der sein Eingang geschützt durch das Notausrelay gesteuert wird. 

## Stepper Motoren
### Signalfunktionen
Die Treiber haben einen Eingang für das Zwanghafte freilaufen
### Sicherheit
das Signal für das Zwanghafte freilaufen wird gegeben und die Motorspannung wird getrennt. Da der Motortreiber bei einem Spannungsverlust einen Reset durchführt, werden die alle Spannungen getrennt und der Treiber muss nach dem reaktivieren mit neuer Konfig versorgt werden. Der Treiber erhält nach erfolgreichen rekonfigurieren das Zwanghafte freilaufen Signal entfernt.
1. Zwanghafte Freilaufen wird gesetzt
2. Alle Spannungsversorgungen werden getrennt
